import { defineStore } from "pinia";

export const useCounterStore = defineStore("counter", {
  state: () => ({
    counter: 0,
    fullName: "",
  }),
  getters: {
    doubleCount: (state) => state.counter * 2,
    showName: (state) => state.fullName,
  },
  actions: {
    increment() {
      this.counter++;
    },
    setName(state, payload) {
      this.fullName = payload;
    },
    reset() {
      this.counter = 0;
    },
  },
});
